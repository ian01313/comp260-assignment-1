﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

	//the transform of the object to follow
	public Transform target;

	private NavMeshAgent agent;

	void Start () {
	//get a reference to the NavMeshAgent component
		agent = GetComponent<NavMeshAgent>();
	}

	void Update () {
	//tell the agent to go to the target's position
		agent.SetDestination (target.position);
	}
}
